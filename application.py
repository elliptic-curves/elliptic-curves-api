from flask import Flask, make_response, jsonify, request
from flask_cors import CORS

import numpy as np
import matplotlib.pyplot as plt
import urllib
import base64
import io

'''
'curve': {'equation': {'string': 'y^2 = x^3 + 7', 'x3': 1, 'x2': 0, 'ax': 0, 'b': 7}}}
'''
def calc_elliptic(curve = None):
    a = -1
    b = 1
    y, x = np.ogrid[-5:5:100j, -5:5:100j]
    if not curve:
        plt.contour(x.ravel(), y.ravel(), pow(y, 2) - pow(x, 3) - x * a - b, [0])
    else:
        equation = curve['equation']
        print(equation['string'])
        plt.contour(x.ravel(), y.ravel(), pow(y, 2) -
                    int(equation['x3'])*pow(x, 3) - int(equation['x2'])*pow(x, 2) - x * int(equation['ax']) - int(equation['b']), [0])
    plt.grid()
    buf = io.BytesIO()
    plt.savefig("test.png")
    plt.savefig(buf, format='png')
    plt.clf()
    buf.seek(0)
    string = base64.b64encode(buf.read()).decode('utf-8')
    return string

app = Flask(__name__)
CORS(app)
@app.route("/get_image", methods=['GET'])
def get_image():
    string = calc_elliptic()
    return_data = {"photo": string}
    return make_response(jsonify(return_data), 200)


@app.route("/get_image2", methods=['POST'])
def get_image2():
    data = request.get_json()
    string = calc_elliptic(data['curve'])
    return_data = {"photo": string}
    return make_response(jsonify(return_data), 200)


if __name__ == "__main__":
    app.run(debug=True)
